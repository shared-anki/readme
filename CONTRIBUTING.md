# Contribution Guide

Currently this is a loosely collected list of tips and conventions

- All decks carry the suffix `_git` to differentiate them from locally managed decks
- The CrowdAnki git import requires the repository name and the deck name to be the same
- The CrowdAnki git import requires the `<deck name>.json` file to be at the top level of the repo

If you want to contribute your own deck, please open an issue in this repository.

