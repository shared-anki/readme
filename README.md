# Shared Anki Decks

## Available Decks
Module                                                                                | Language
--------------------------------------------------------------------------------------|-----------
[Datenkommunikation](https://git.rwth-aachen.de/shared-anki/Datenkommunikation_git)   | German


## CrowdAnki Installation (Prerequisite)
To import to/from git [CrowdAnki](https://github.com/Stvad/CrowdAnki) is used.
For installation enter the add-on code `1788670778` or go to the [install page](https://ankiweb.net/shared/info/1788670778)

## Importing an Anki Deck
1. Navigate to `File > CrowdAnki: Import git repository`
2. Enter the git url of the deck
3. Finish the import with the Anki import assistant

You're invited to contribute changes or even your own deck.
